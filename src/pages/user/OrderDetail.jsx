/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Card, Divider, Button, Breadcrumb , Modal} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';
import ReceiverInfo from './OrderDetail/ReceiverInfo'
import PaymentInfo from './OrderDetail/PaymentInfo'
import DeliveryInfo from './OrderDetail/DeliveryInfo'
import InvoiceInfo from './OrderDetail/InvoiceInfo'
import ItemList from './OrderDetail/ItemList'

import {Link} from 'react-router-dom'

import HttpUtils from 'utils/HttpUtils'

const confirm = Modal.confirm;

class OrderDetail extends React.Component {

    state = {
        payment: {},
        order: {}
    }

    componentDidMount() {
        let self = this;
        let orderId = this.props.match.params.id;

        HttpUtils.getOrder({id: orderId}, {
            success(resp){
                if (resp.data) {
                    self.setState({order: resp.data})
                }
            },
            error(resp){

            }
        })
    }

    showConfirm() {

        confirm({
            title: '是否确定取消订单?',
            cancelText:'取消',
            okText:'确认',
            onOk() {
              this.cancelOrder()
            },
            onCancel() {
              console.log('Cancel');
            },
        });
    }

    cancelOrder(){
        let self = this;
        let orderId = this.props.match.params.id;
        HttpUtils.cancelOrder({id: orderId}, {
            success(resp){
                if (resp.data) {
                    self.setState({order: resp.data})
                }
            },
            error(resp){

            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Breadcrumb>
                            <Breadcrumb.Item><Link to="/center">个人中心</Link></Breadcrumb.Item>
                            <Breadcrumb.Item><Link to="/myorders">订单中心</Link></Breadcrumb.Item>
                            <Breadcrumb.Item>订单：123456</Breadcrumb.Item>
                        </Breadcrumb>
                        <Row style={{'marginTop': '30px', 'backgroundColor': '#fbfbfb' , borderRight : '1px solid #f1f1f1'}}>
                            <Col span={8}>
                                <p className="text-center">
                                    订单号：{this.state.order.id}
                                </p>
                                <h3 className="text-center">
                                    等待付款
                                </h3>
                                <p className="text-center">
                                    <Button><Link to={"/pay?orderId=" + this.state.order.id}>立即支付</Link></Button>
                                </p>
                                <p className="text-center">
                                    <Button onClick={() => this.showConfirm()}>
                                      取消订单
                                    </Button>
                                </p>
                            </Col>
                            <Col span={16}>
                                <span>订单已经完成，感谢您在京东商城购物，欢迎您对本次交易及所购商品进行评价。</span>
                            </Col>
                        </Row>

                        <Row style={{marginTop: '30px', height: '200px'}} gutter={15}>
                            <Col span={6}>
                                <ReceiverInfo/>
                            </Col>
                            <Col span={6}>
                                <PaymentInfo/>
                            </Col>
                            <Col span={6}>
                                <DeliveryInfo/>
                            </Col>
                            <Col span={6}>
                                <InvoiceInfo/>
                            </Col>
                        </Row>

                        <Row>
                            <ItemList/>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default OrderDetail;