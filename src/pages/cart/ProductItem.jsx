/**
 * Created by lichong on 3/9/2018.
 */

import React from 'react';
import { Badge, Button, Dropdown, Menu } from 'antd';

import {Link} from 'react-router-dom'

import './ProductItem.css'

class ProductItem extends React.Component {

    componentDidMount() {

    }

    addProductToCart(){
        this.props.addProductToCart(this.props.data.prodId);
    }

    render() {
        return (
            <div className="prod-item">
                <img className="prod-img" src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"/>
                <p className="prod-name" >{this.props.data.prodName}</p>
                <p className="prod-price">$12.00</p>
                <div>
                    <Button onClick={() => this.addProductToCart()}>加入购物车</Button>
                </div>
            </div>
        );
    }
}

export default ProductItem;