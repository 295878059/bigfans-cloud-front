/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Row, Col, Radio} from 'antd';

import {Link , withRouter} from 'react-router-dom'

import HttpUtils from '../../utils/HttpUtils';

class Sku extends React.Component {
    state = {
        sku : {
            specGroups : []
        },
    };

    componentDidMount() {
        var self = this;
        HttpUtils.getSku({id: this.props.prodId}, {
            success: function (response) {
                if(response.data){
                    self.setState({sku: response.data})
                }
            }
        })
    }

    selectProduct(sv){
        window.location.href="/product/"+sv.prodId;
    }

    render() {
        return (
            <div>
                {
                    this.state.sku.specGroups.map((group, index) => {
                        return (
                            <Row style={{marginTop: '10px'}} key={group.option.id}>
                                <Col span={3}>
                                    选择{group.option.name}
                                </Col>
                                <Col span={21}>
                                    <Radio.Group >
                                        {
                                            group.values.map((sv , index) =>{
                                                return (
                                                    <Radio.Button onClick={()=>this.selectProduct(sv)} value={sv.valueId} key={sv.valueId} checked={sv.selected} disabled={!sv.selectable}>
                                                            {sv.value}
                                                    </Radio.Button>
                                                )
                                            })
                                        }
                                    </Radio.Group>
                                </Col>
                            </Row>
                        )
                    })
                }
            </div>
        );
    }
}
export default withRouter(Sku);